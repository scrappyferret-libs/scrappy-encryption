--- Required libraries.
local openssl = require( "plugin.openssl" )
local mime = require( "mime" )
local json = require( "json" )

-- Localised functions.
local encode = json.encode
local decode = json.decode
local b64 = mime.b64
local unb64 = mime.decode

-- Localised values

--- Class creation.
local library = {}

--- Initialises this Scrappy library.
-- @param params The params for the initialisation.
function library:init( params )

	-- Store out the params, if any
	self._params = params or {}

	-- Set the cipher suite
	self:setCipherSuite( self._params.cipherSuite or "aes-256-cbc" )

end

--- Sets the cipher suite for the system to use.
-- @param type The suite to set. Possibles can be found here https://www.openssl.org/docs/manmaster/apps/ciphers.html
function library:setCipherSuite( type )

	self._ciperSuite = type

	if openssl then
		self._cipher = openssl.get_cipher( self._ciperSuite )
	end

end

--- Gets the cipher suite that the system is using.
-- @return The name of the suite.
function library:getCipherSuite()
	return self._ciperSuite
end

--- Encrypts some data.
-- @param data The data to encrypt.
-- @param key The encryption key to use.
-- @return The encrypted data.
function library:encrypt( data, key )

    if type( data ) == "table" then
        data = encode( data )
    end

    return b64( self._cipher:encrypt ( data, key ) )

end

--- Decrypts some data.
-- @param data The data to decrypt.
-- @param key The encryption key to use.
-- @return The decrypted data.
function library:decrypt( data, key )

	local data = self._cipher:decrypt( unb64( data ), key )

	-- Try to decode the data into a json table
	local temp = decode( data )
	if temp then
		data = temp
	end

 	return data

end

--- Destroys this library.
function library:destroy()

end

-- If we don't have a global Scrappy object i.e. this is the first Scrappy plugin to be included
if not Scrappy then

	-- Create one
	Scrappy = {}

end

-- If we don't have a Scrappy Encryption library
if not Scrappy.Encryption then

	-- Then store the library out
	Scrappy.Encryption = library

end

-- Return the new library
return library
